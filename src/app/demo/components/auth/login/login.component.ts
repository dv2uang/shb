import {Component, OnInit} from '@angular/core';
import { LayoutService } from 'src/app/layout/service/app.layout.service';
import {AuthService} from "./auth.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import {Route, Router} from "@angular/router";
import {MessageService} from "primeng/api";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styles: [`
        :host ::ng-deep .pi-eye,
        :host ::ng-deep .pi-eye-slash {
            transform:scale(1.6);
            margin-right: 1rem;
            color: var(--primary-color) !important;
        }
    `]
})
export class LoginComponent implements OnInit{

    valCheck: string[] = ['remember'];
    password!: string;
    form!: FormGroup;
    errorsMsg: any;
    constructor(
        public layoutService: LayoutService,
        private authService: AuthService,
        private fb: FormBuilder,
        private route: Router,
        private messageService: MessageService
        ) { }

    ngOnInit(): void {
        this.initForm();
    }

    initForm():void{
        this.form = this.fb.group({
             username: [null],
             password: [null]
        })
    }
    doLogin() {
        // console.log(this.form.value)
        this.errorsMsg = [];
        this.authService.login(this.form.value).subscribe(res => {
            // console.log(res);
            this.authService.setToken(res.access_token);
            this.route.navigate(['']);
        }, err => {
            console.log(err)
             this.messageService.add({severity:'error', detail:err.error.message});
        })
    }
}