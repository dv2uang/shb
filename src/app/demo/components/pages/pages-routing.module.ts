import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [RouterModule.forChild([
        { path: 'crud', loadChildren: () => import('./cif-management/crud.module').then(m => m.CrudModule) },
        { path: 'empty', loadChildren: () => import('./cif-create/emptydemo.module').then(m => m.EmptyDemoModule) },
        { path: 'timeline', loadChildren: () => import('./account-management/timelinedemo.module').then(m => m.TimelineDemoModule) },
        { path: 'account-create', loadChildren: () => import('./account-create/account-create.module').then(m => m.AccountCreate) },
        { path: 'card-create', loadChildren: () => import('./card-create/chartsdemo.module').then(m => m.ChartsDemoModule) },
        { path: 'card-list', loadChildren: () => import('./card-management/filedemo.module').then(m => m.FileDemoModule) },
        { path: '**', redirectTo: '/notfound' }
    ])],
    exports: [RouterModule]
})
export class PagesRoutingModule { }
