import { Component, OnInit } from '@angular/core';
import { PrimeIcons } from 'primeng/api';

@Component({
    templateUrl: './timelinedemo.component.html',
    styleUrls: ['./timelinedemo.scss']
})
export class TimelineDemoComponent implements OnInit {

    customers: any[] = [
        {
            code: '1234567890',
            name: 'Nguyen Thu Thao',
            type: 'tài khoản thanh toán',
            money: 'VND'
        },
        {
            code: '1234567890',
            name: 'Nguyen Thu Thao',
            type: 'tài khoản thanh toán',
            money: 'VND'
        },
        {
            code: '1234567890',
            name: 'Nguyen Thu Thao',
            type: 'tài khoản thanh toán',
            money: 'VND'
        },
        {
            code: '1234567890',
            name: 'Nguyen Thu Thao',
            type: 'tài khoản thanh toán',
            money: 'VND'
        },
        {
            code: '1234567890',
            name: 'Nguyen Thu Thao',
            type: 'tài khoản thanh toán',
            money: 'VND'
        },
    ];

    ngOnInit() {

    }

}
