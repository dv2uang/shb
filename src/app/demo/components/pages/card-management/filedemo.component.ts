import { Component } from '@angular/core';
import { MessageService } from 'primeng/api';

@Component({
    templateUrl: './filedemo.component.html',
    providers: [MessageService]
})
export class FileDemoComponent {
    customers: any[] = [
        {
            code: '1234567890',
            name: 'Nguyen Thu Thao',
            type: 'tài khoản thanh toán',
            money: 'VND'
        },
        {
            code: '1234567890',
            name: 'Nguyen Thu Thao',
            type: 'tài khoản thanh toán',
            money: 'VND'
        },
        {
            code: '1234567890',
            name: 'Nguyen Thu Thao',
            type: 'tài khoản thanh toán',
            money: 'VND'
        },
        {
            code: '1234567890',
            name: 'Nguyen Thu Thao',
            type: 'tài khoản thanh toán',
            money: 'VND'
        },
        {
            code: '1234567890',
            name: 'Nguyen Thu Thao',
            type: 'tài khoản thanh toán',
            money: 'VND'
        },
    ];
    uploadedFiles: any[] = [];

    constructor(private messageService: MessageService) {}

    onUpload(event: any) {
        for (const file of event.files) {
            this.uploadedFiles.push(file);
        }

        this.messageService.add({ severity: 'info', summary: 'Success', detail: 'File Uploaded' });
    }

    onBasicUpload() {
        this.messageService.add({ severity: 'info', summary: 'Success', detail: 'File Uploaded with Basic Mode' });
    }

}
