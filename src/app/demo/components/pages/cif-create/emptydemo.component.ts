import { Component, OnInit } from '@angular/core';

@Component({
    templateUrl: './emptydemo.component.html'
})

export class EmptyDemoComponent implements OnInit {
    selectedValue!: string;


    productDialog: boolean = false;

    deleteProductDialog: boolean = false;

    deleteProductsDialog: boolean = false;

    submitted: boolean = false;

    cols: any[] = [];

    statuses: any[] = [];

    rowsPerPageOptions = [5, 10, 20];

    constructor() { }

    ngOnInit() {
    }

}
