import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmptyDemoRoutingModule } from './emptydemo-routing.module';
import { EmptyDemoComponent } from './emptydemo.component';
import { InputTextModule } from 'primeng/inputtext';
import {DropdownModule} from 'primeng/dropdown';
import {CalendarModule} from 'primeng/calendar';
import { RadioButtonModule } from 'primeng/radiobutton';
import { FormsModule } from '@angular/forms';
import {ButtonModule} from 'primeng/button';

@NgModule({
    imports: [
        CommonModule,
        EmptyDemoRoutingModule,
        InputTextModule,
        DropdownModule,
        CalendarModule,
        RadioButtonModule,
        FormsModule,
        ButtonModule
    ],
    declarations: [EmptyDemoComponent]
})
export class EmptyDemoModule { }
